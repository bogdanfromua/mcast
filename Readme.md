### TODO
- deb add script for   
```bash
ln -sf "/opt/mcast-1.0-SNAPSHOT/mcast-1.0-SNAPSHOT" "/usr/bin/mcast"
# ln -sf "/opt/mcast-1.0-SNAPSHOT/mcast-1.0-SNAPSHOT" "/media/user/Data/SyncSpace/git/mcast/src/main/deploy/package/linux/usr/bin/mcast"
```
- printing expand to submenu if find good match (full word/first letter of word)
- save artifacts in artifacts storage
- change application title
- calculate initial menu position from monitor size or mouse position
- add license (free, not aggressive)
- fix TODOs in code
- make filtering more smart
- TODO: use public artifact storage




### Build
TODO: add instruction for build deb package form another OS
```bash
./gradlew jfxNative
curl -uadmin:<ARTIFACTORY_PASSWORD> -T ./nativeArtifact/mcast-1.0-snapshot-1.0.deb "https://plishchenko.com:8445/mcast/mcast-1.0-snapshot.deb"
```
