package com.plishchenko.mcast;

import lombok.Data;

import java.io.File;

@Data
public class RunParameters {
    private String title;
    private File root;
    private int positionX;
    private int positionY;
    private String arguments;
    private File subjectFile;
}
