package com.plishchenko.mcast;

import com.plishchenko.mcast.interaction.MenuPanel;
import com.plishchenko.mcast.model.scanners.FilesystemScanner;
import com.plishchenko.mcast.model.NodesModel;
import com.plishchenko.mcast.model.scanners.NodesSourceScanner;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.commons.lang3.tuple.ImmutablePair;

import static com.plishchenko.mcast.InitialParametersResolver.resolveRunParameters;
import static com.plishchenko.mcast.interaction.MenuPanel.showMenuPanel;

public class McastApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        RunParameters runParameters = resolveRunParameters(getParameters());

        NodesSourceScanner nodesSourceScanner = new FilesystemScanner(runParameters.getRoot());
        NodesModel nodesModel = new NodesModel(nodesSourceScanner);
        MenuPanel menuPanel = showMenuPanel(
                primaryStage,
                nodesModel,
                runParameters.getTitle(),
                new ImmutablePair<>(runParameters.getPositionX(), runParameters.getPositionY()),
                null,
                runParameters.getArguments(),
                runParameters.getSubjectFile());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
