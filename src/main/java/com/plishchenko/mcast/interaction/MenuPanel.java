package com.plishchenko.mcast.interaction;

import com.plishchenko.mcast.model.FilterableModel;
import com.plishchenko.mcast.model.NodesModel;
import com.plishchenko.mcast.model.beans.Node;
import com.plishchenko.mcast.textaccumulator.TextAccumulator;
import com.sun.javafx.collections.ObservableListWrapper;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.List;

import static javafx.stage.StageStyle.UNDECORATED;

public class MenuPanel {
    final static int ROW_HEIGHT = 24;
    final static int SUBMENU_DISTANCE_X = 2;

    private final ActionExecutor actionExecutor;

    private KeyHandler keyHandler;
    private Stage stage;

    private Pane rootPane;
    private Scene scene;
    private ListView<Node> menu;
    private final FilterableModel filterableModel;
    @Nullable
    private MenuPanel parentMenu;

    private MenuPanel(Stage stage,
                      String title,
                      NodesModel nodesModel,
                      Pair<Integer, Integer> position,
                      @Nullable MenuPanel parentMenu,
                      @Nullable String arguments,
                      @Nullable File subjectFile) {
        this.parentMenu = parentMenu;
        // initialize non gui services
        this.actionExecutor = new ActionExecutor(this, arguments, subjectFile);

        // initialize and wire gui base components together
        this.stage = stage;
        this.rootPane = new Pane();
        this.scene = new Scene(rootPane);
        stage.setScene(scene);

        // configure window outer look
        stage.setTitle(title);
        stage.initStyle(UNDECORATED);
        stage.setX(position.getLeft());
        stage.setY(position.getRight());

        // initialize menu component
        this.menu = new ListView<>();
        menu.setCellFactory(list -> new McastNodeListCell());
        this.keyHandler = new KeyHandler(this, actionExecutor);
        menu.setOnKeyPressed(keyHandler::handleKeyEvent);

        // initialize menu items

        filterableModel = new FilterableModel(nodesModel, this);

        filterableModel.addObserver((List<Node> nodes, Integer selectedNodeIndex) -> {
            menu.setItems(new ObservableListWrapper<>(nodes));
            menu.getSelectionModel().selectIndices(selectedNodeIndex);
        });

        filterableModel.forceNotify();

        filterableModel.addObserver((List<Node> nodes, Integer index) -> {
            resizeScene();
        });

        filterableModel.forceNotify();



        TextAccumulator textAccumulator = new TextAccumulator(filterableModel::handleFilterUpdate);
        this.keyHandler.setTextFilterAcumulator(textAccumulator);
        // TODO: add text filter current state visualization

//        Button btn = new Button();
//        btn.setText("Say 'Hello World'");
//        btn.setOnAction(event ->
//                System.out.println("Hello World!"));
//        rootPane.getChildren().add(btn);


        // add gui components to gui base
        rootPane.getChildren().add(menu);

        // show
        resizeScene();
        makeFirstItemSelected();
        stage.show();
    }

    public static MenuPanel showMenuPanel(@Nullable Stage stage,
                                          NodesModel nodesModel,
                                          String title,
                                          Pair<Integer, Integer> position,
                                          @Nullable MenuPanel parentMenu,
                                          @Nullable String arguments,
                                          @Nullable File subjectFile) {
        if (stage == null) {
            stage = new Stage();
        }
        return new MenuPanel(stage, title, nodesModel, position, parentMenu, arguments, subjectFile);
    }

    private void resizeScene() {
//        System.out.println("ttttest  " + menu.getItems().size());
        int requiredHeight = menu.getItems().size() * ROW_HEIGHT + 2;
//        System.out.println("requiredHeight  " + requiredHeight);
        menu.setPrefHeight(requiredHeight);
        rootPane.setPrefHeight(requiredHeight);
//        System.out.println("menu height  " + menu.getHeight());
//        System.out.println("pane height  " + rootPane.getHeight());
        stage.setHeight(requiredHeight);
    }

    public void close() {
        stage.close();
    }

    public Pair<Integer, Integer> calculateSubmenuPosition(Node currentNode) {
        int positionX = (int) (stage.getWidth() + stage.getX() + SUBMENU_DISTANCE_X);
        int positionY = (int) (stage.getY() + currentNode.getCell().getLayoutY());
        return new ImmutablePair<>(positionX, positionY);
    }

    public Node getSelectedItem() {
        return menu.getSelectionModel().getSelectedItem();
    }

    private void makeFirstItemSelected() {
        menu.getSelectionModel().select(0);
    }

    public MenuPanel getParentMenu() {
        return parentMenu;
    }

    public ActionExecutor getActionExecutor() {
        return actionExecutor;
    }
}
