package com.plishchenko.mcast.interaction;

import com.plishchenko.mcast.model.NodesModel;
import com.plishchenko.mcast.model.beans.Node;
import com.plishchenko.mcast.model.scanners.FilesystemScanner;
import com.plishchenko.mcast.model.scanners.NodesSourceScanner;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.Nullable;

import java.io.File;

import static com.plishchenko.mcast.interaction.MenuPanel.showMenuPanel;
import static com.plishchenko.mcast.osinteraction.CommandsExecutor.executeFile;

public class ActionExecutor {

    private MenuPanel menuPanel;
    private String arguments;
    private File subjectFile;

    public ActionExecutor(MenuPanel menuPanel, @Nullable String arguments, @Nullable File subjectFile) {
        this.menuPanel = menuPanel;
        this.arguments = arguments;
        this.subjectFile = subjectFile;
    }

    public void executeCommand(Node currentNode) {
        closeAllMenus();
        executeFile(currentNode.getExecutableFile(), arguments, subjectFile);
    }

    public void openSubMenu(Node currentNode) {
        Pair<Integer, Integer> position = menuPanel.calculateSubmenuPosition(currentNode);

        File submenuFolder = new File(currentNode.getChildrenReference());
        NodesSourceScanner nodesSourceScanner = new FilesystemScanner(submenuFolder);
        NodesModel nodesModel = new NodesModel(nodesSourceScanner);
        MenuPanel menuPanel = showMenuPanel(null, nodesModel, currentNode.getCaption(), position, this.menuPanel, arguments, subjectFile);
    }

    public void closeCurrentMenu() {
        menuPanel.close();
    }

    public void closeAllMenus() {
        System.out.println("closeAllMenus");
        MenuPanel parentMenu = menuPanel.getParentMenu();

        menuPanel.close();

        if (parentMenu != null) {
            parentMenu.getActionExecutor().closeAllMenus();
        }
    }

    public void executeCommandOrOpenSubMenu(Node currentNode) {
        if (!StringUtils.isBlank(currentNode.getChildrenReference())) {
            openSubMenu(currentNode);
        } else {
            executeCommand(currentNode);
        }
    }
}
