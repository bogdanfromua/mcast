package com.plishchenko.mcast.interaction;

import com.plishchenko.mcast.model.beans.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import org.apache.commons.lang3.StringUtils;

public class McastNodeListCell extends ListCell<Node> {

    private Node node;

    static private String resolveNodeCaption(Node node) {
        return node.getCaption();
    }

    static private boolean haveSubitems(Node node) {
        return !StringUtils.isEmpty(node.getChildrenReference());
    }

    static javafx.scene.Node generateGraphicNode(Node node) {
        StringBuilder finalCaption = new StringBuilder(resolveNodeCaption(node));
        if (haveSubitems(node)) {
            finalCaption.append("   >>");
            // TODO: move this mark to the right side
        }
        return new Label(finalCaption.toString());
    }

    @Override
    protected void updateItem(Node node, boolean empty) {
        super.updateItem(node, empty);
        this.node = node;

        if (empty || node == null) {
            return;
        }

        node.setCell(this);
        updateGraphicNode();
    }

    @Override
    protected boolean isItemChanged(Node oldItem, Node newItem) {
        return true;
    }

//    public void update() {
//        updateGraphicNode();
//    }

    private void updateGraphicNode() {
        setGraphic(generateGraphicNode(this.node));
    }

}