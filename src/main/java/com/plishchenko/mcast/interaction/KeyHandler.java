package com.plishchenko.mcast.interaction;

import com.plishchenko.mcast.model.beans.Node;
import com.plishchenko.mcast.textaccumulator.TextAccumulator;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import static javafx.scene.input.KeyCode.*;

class KeyHandler {
    private MenuPanel menuPanel;
    private ActionExecutor actionExecutor;
    private TextAccumulator textFilterAcumulator;

    public KeyHandler(MenuPanel menuPanel, ActionExecutor actionExecutor) {
        this.menuPanel = menuPanel;
        this.actionExecutor = actionExecutor;
    }

    public void handleKeyEvent(KeyEvent event) {
        Node currentNode = menuPanel.getSelectedItem();
        KeyCode keyCode = event.getCode();
        String text = event.getText();

        if (ESCAPE.equals(keyCode)) {
            if (!textFilterAcumulator.isEmpty()) {
                textFilterAcumulator.clear();
            } else {
                actionExecutor.closeCurrentMenu();
            }
        }
        if (LEFT.equals(keyCode)) {
            actionExecutor.closeCurrentMenu();
        }
        if (BACK_SPACE.equals(keyCode)) {
            textFilterAcumulator.pop();
        }
        if (ENTER.equals(keyCode)) {
            actionExecutor.executeCommandOrOpenSubMenu(currentNode);
        }
        if (RIGHT.equals(keyCode)) {
            actionExecutor.openSubMenu(currentNode);
        }
        if (keyCode.isLetterKey() || keyCode.isDigitKey()) {
            appendTextFilter(text);
        }
    }

    private void appendTextFilter(String aChar) {
        textFilterAcumulator.push(aChar);
    }

    public void setTextFilterAcumulator(TextAccumulator textFilterAcumulator) {
        this.textFilterAcumulator = textFilterAcumulator;
    }
}
