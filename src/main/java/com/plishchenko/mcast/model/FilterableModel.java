package com.plishchenko.mcast.model;

import com.plishchenko.mcast.interaction.MenuPanel;
import com.plishchenko.mcast.model.beans.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import static java.util.stream.Collectors.toList;

public class FilterableModel {
    private List<BiConsumer<List<Node>, Integer>> observers = new ArrayList<>();
    private NodesModel originalModel;
    private MenuPanel menuPanel;
    private List<Node> visibleNodes;
    private Node lastSecededNode = null;

    public FilterableModel(NodesModel originalModel, MenuPanel menuPanel) {
        this.originalModel = originalModel;
        this.menuPanel = menuPanel;
        this.visibleNodes = originalModel.getNodes();
    }

    private boolean isNodeMatchFilter(Node node, String filter) {
        if (filter.isEmpty()) {
            return true;
        }
        // TODO: make filtering more smart
        return node.getCaption().toLowerCase().startsWith(filter.toLowerCase());
    }

    public void handleFilterUpdate(String filter) {
        if (menuPanel.getSelectedItem() != null) {
            lastSecededNode = menuPanel.getSelectedItem();
        }
        visibleNodes = originalModel.getNodes().stream()
                .filter(node -> isNodeMatchFilter(node, filter))
                .collect(toList());

        notifyObservers();
    }

    private int resolveCurrentSelectedIndex() {
        return visibleNodes.indexOf(lastSecededNode);
    }

    public void forceNotify() {
        notifyObservers();
    }

    private List<Node> resolveCurrentFilterState() {
        return visibleNodes;
    }

    private void notifyObserver(BiConsumer<List<Node>, Integer> observer) {
        observer.accept(
                resolveCurrentFilterState(),
                resolveCurrentSelectedIndex());
    }

    private void notifyObservers() {
        observers.forEach(this::notifyObserver);
    }

    public void addObserver(BiConsumer<List<Node>, Integer> observer) {
        observers.add(observer);
    }
}
