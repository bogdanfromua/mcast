package com.plishchenko.mcast.model.scanners;

import com.plishchenko.mcast.model.beans.Node;

import java.util.List;

public interface NodesSourceScanner {
    List<Node> scanRoot();
}
