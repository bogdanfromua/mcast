package com.plishchenko.mcast.model.scanners;

import com.plishchenko.mcast.model.beans.Node;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;

//@Slf4j
public class FilesystemScanner implements NodesSourceScanner {

    // TODO: configure Slf4j
    // TODO: check for symlinks loops
    // TODO: make it possible to scan symlinks (if it don't work by default)

    @NotNull
    private File directory;

    public FilesystemScanner(@NotNull File directory) {
        this.directory = directory;
    }

    @Override
    public List<Node> scanRoot() {
        if (!directory.exists()) {
            throw new IllegalArgumentException("Commands root folder " + directory.getPath() + " does not exist");
        }
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException("Commands root must be a directory");
        }
        return scanDirContent(directory);
    }

    private List<Node> scanDirContent(@NotNull File directory) {
        List<Node> nodes = new ArrayList<>();
        for (File file : directory.listFiles()) {
            nodes.add(scanFilesystemItem(file));
        }
        return nodes;
    }

    private Node scanFilesystemItem(@NotNull File file) {
        Node node = new Node(file.getName());
        if (file.isDirectory()) {
            node.setChildrenReference(file.getAbsolutePath());
        } else if (file.isFile() && file.canExecute()) {
            node.setExecutableFile(file);
        } else if (file.isFile() && !file.canExecute()) {
//            log.warn("File found but can not be executed");
        } else {
            throw new IllegalStateException();
        }
        return node;
    }
}
