package com.plishchenko.mcast.model;

import com.plishchenko.mcast.model.beans.Node;
import com.plishchenko.mcast.model.scanners.NodesSourceScanner;

import java.util.List;

public class NodesModel {
    private List<Node> nodes;

    public NodesModel(NodesSourceScanner nodesSourceScanner) {
        nodes = nodesSourceScanner.scanRoot();
    }

    public List<Node> getNodes() {
        return nodes;
    }
}
