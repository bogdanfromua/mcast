package com.plishchenko.mcast.model.beans;

import com.plishchenko.mcast.interaction.McastNodeListCell;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class Node {
    private String caption;
    private File executableFile;
    private String childrenReference;
    private McastNodeListCell cell;

    public Node(String caption) {
        this.caption = caption;
    }

    public String getCaption() {
        return caption;
    }

    public void setExecutableFile(@NotNull File executableFile) {
        this.executableFile = executableFile;
    }

    public File getExecutableFile() {
        return executableFile;
    }

    public void setChildrenReference(String childrenReference) {
        this.childrenReference = childrenReference;
    }

    public String getChildrenReference() {
        return childrenReference;
    }

    public void setCell(McastNodeListCell cell) {
        this.cell = cell;
    }

    public McastNodeListCell getCell() {
        return cell;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
