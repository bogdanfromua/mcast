package com.plishchenko.mcast;

import javafx.application.Application;
import javafx.application.Application.Parameters;

import java.io.File;
import java.util.Map;

import static java.util.Optional.ofNullable;

public class InitialParametersResolver {
    public static final String DEFAULT_MCAST_ROOT = "/home/user/mcast/root/";   // TODO: replace it by flexible paths model
    public static final String DEFAULT_MCAST_TITLE = "menu";   // TODO: replace it after adding support for parameter keys
    public static final int DEFAULT_POSITION_X = 300;
    public static final int DEFAULT_POSITION_Y = 500;

    static RunParameters resolveRunParameters(Parameters parameters) {
        RunParameters runParameters = new RunParameters();

        Map<String, String> namedParams = parameters.getNamed();

        runParameters.setTitle(
                ofNullable(namedParams.get("title"))
                        .orElse(DEFAULT_MCAST_TITLE));

        runParameters.setRoot(
                ofNullable(namedParams.get("root"))
                        .map(File::new)
                        .orElse(new File(DEFAULT_MCAST_ROOT)));

        runParameters.setPositionX(
                ofNullable(namedParams.get("positionx"))
                        .map(Integer::parseInt)
                        .orElse(DEFAULT_POSITION_X));

        runParameters.setPositionY(
                ofNullable(namedParams.get("positiony"))
                        .map(Integer::parseInt)
                        .orElse(DEFAULT_POSITION_Y));

        runParameters.setArguments(namedParams.get("arguments"));

        // subject file
        if (parameters.getUnnamed().size() != 0) {
            runParameters.setSubjectFile(new File(parameters.getUnnamed().get(0)));
        }

        return runParameters;
    }
}
