package com.plishchenko.mcast.osinteraction;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.plishchenko.mcast.osinteraction.ProcessUtils.runAsynchronously;
import static java.util.Arrays.asList;

public class CommandsExecutor {

    public static void executeFile(File executableFile, @Nullable String arguments, @Nullable File subjectFile) {
        try {
            List<String> commandList = new ArrayList<>();
            commandList.addAll(asList("sh", "-c"));

            StringBuilder sb = new StringBuilder(executableFile.getPath());
            if (arguments != null) {
                sb.append(" ").append(arguments);
            }
            if (subjectFile != null) {
                sb.append(" ").append(subjectFile.getPath());
            }

            commandList.add(sb.toString());
            runAsynchronously(commandList, "mcastExecuteLogs");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
