package com.plishchenko.mcast.osinteraction;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProcessUtils {
//    private static final Logger logger = LoggerFactory.getLogger(ProcessUtils.class);

    private static final int OS_PROPERTIES_LOGGING_MS_INTERVAL = 1000 * 10;

    public enum LogLevel {
        SYSOUT,
        DEBUG,
        INFO,
        NONE
    };

    /**
     * Run external process using specified command.
     * ignore stdout, stderr
     *
     * @param command the list containing the program and its arguments.
     * @return return code.
     */
    public static int runSynchronously(List<String> command, String tmpDirPrefix) throws IOException, InterruptedException {
        // create temp log file
        Path tempDirectory = Files.createTempDirectory(tmpDirPrefix);
        Path logFile = tempDirectory.resolve("log.log").toFile().toPath();

        return runSynchronously(command, logFile);
    }

    /**
     * Run external process using specified command.
     *
     * @param command the list containing the program and its arguments.
     * @param logFile file for redirect stdout, stderr.
     * @return return code.
     */
    public static int runSynchronously(List<String> command, Path logFile) throws IOException, InterruptedException {
//        logger.info("Run command: {}", command);
//        logger.info("io: log file: {}", logFile.toString());

        // exec
        ProcessBuilder pBuilder = new ProcessBuilder();
        pBuilder.command(command);
        pBuilder.redirectError(logFile.toFile());
        pBuilder.redirectOutput(logFile.toFile());
        Process process = pBuilder.start();

        // wait
        int exitCode = process.waitFor();

        return exitCode;
    }

    /**
     * Run external process using specified command.
     * Redirect current stdin to running process;
     * Redirect running process stdout, stderr from running process to current stdout, stderr
     *
     * @param command the list containing the program and its arguments.
     * @return return code.
     */
    public static int runSynchronously(List<String> command) throws IOException, InterruptedException {
//        logger.info("Run command: {}", command);
//        logger.info("io: inherited from parent process");

        // exec
        ProcessBuilder pBuilder = new ProcessBuilder();
        pBuilder.command(command);
        pBuilder.inheritIO();
        Process process = pBuilder.start();

        // wait
        int exitCode = process.waitFor();

        return exitCode;
    }

    /**
     * Run external process using specified command.
     * ignore stdout, stderr
     *
     * @param command the list containing the program and its arguments.
     */
    public static void runAsynchronously(List<String> command, String tmpDirPrefix) throws IOException, InterruptedException {
        // create temp logrunAsynchronously file
        Path tempDirectory = Files.createTempDirectory(tmpDirPrefix);
        Path logFile = tempDirectory.resolve("log.log").toFile().toPath();

        runAsynchronously(command, logFile);
    }

    /**
     * Run external process using specified command.
     *
     * @param command the list containing the program and its arguments.
     * @param logFile file for redirect stdout, stderr.
     */
    public static void runAsynchronously(List<String> command, Path logFile) throws IOException, InterruptedException {
        ProcessBuilder pBuilder = new ProcessBuilder();

        System.out.println();
        command.forEach(it -> System.out.print(it + " : "));

        pBuilder.command(command);
        pBuilder.redirectError(logFile.toFile());
        pBuilder.redirectOutput(logFile.toFile());
        pBuilder.start();
    }

    /**
     * Run external process using specified command.
     * Redirect current stdin to running process;
     * Redirect running process stdout, stderr from running process to current stdout, stderr
     *
     * @param command the list containing the program and its arguments.
     * @param logLevel specifies what level of logging will be used for runned command output.
     * @return return code.
     */
    public static int runSynchronously(List<String> command, LogLevel logLevel) throws IOException, InterruptedException {
//        logger.info("Run command: {}", command);
//        logger.info("io: stdout, logLevel-{}", logLevel.toString());

        // exec
        ProcessBuilder pBuilder = new ProcessBuilder();
        pBuilder.command(command);
        pBuilder.redirectErrorStream(true);
        Process process = pBuilder.start();

        // wait for finish and redirect process output
        BufferedReader processOutputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        int ramLogCounter = 0;
        while (process.isAlive()) {

            // log process output
            String processOutputLine = null;
            while ((processOutputLine = processOutputReader.readLine()) != null) {
                if (LogLevel.DEBUG == logLevel) {
//                    logger.debug(processOutputLine);
                } else if (LogLevel.INFO == logLevel) {
//                    logger.info(processOutputLine);
                } else if (LogLevel.SYSOUT == logLevel) {
                    System.out.println(processOutputLine);
                }
            }

            // log OS properties
            ramLogCounter = ramLogCounter + 10;
            if (ramLogCounter > OS_PROPERTIES_LOGGING_MS_INTERVAL) {
                ramLogCounter = 0;
                logOSFreeRam(logLevel);
            }

            Thread.sleep(10);
        }

        int exitCode = process.exitValue();
//        logger.info("Command finished execution with exit code = {}", exitCode);
        return exitCode;
    }

    public static int runExecutableSynchronously(Path executablePath, LogLevel logLevel, List<String> options) throws IOException, InterruptedException {
        // build command
        List<String> command = new ArrayList<>();
        command.add(executablePath.toString());
        options.forEach(command::add);

        // run
        return runSynchronously(command, logLevel);
    }

    public static int runJarSynchronously(Path jarPath, LogLevel logLevel, List<String> javaOptions, List<String> jarOptions) throws IOException, InterruptedException {
        // build command
        List<String> command = new ArrayList<>();
        command.add("java");
        javaOptions.forEach(command::add);
        command.add("-jar");
        command.add(jarPath.toString());
        jarOptions.forEach(command::add);

        // run
        return runSynchronously(command, logLevel);
    }

    public static int runJarSynchronously(Path jarPath, LogLevel logLevel, Integer xmxG, List<String> jarOptions) throws IOException, InterruptedException {
        String xmxOption = "-Xmx" + Integer.toString(xmxG) + "G";
        List<String> javaOptions = Collections.singletonList(xmxOption);
        return runJarSynchronously(jarPath, logLevel, javaOptions, jarOptions);
    }

    private static void logOSFreeRam(LogLevel logLevel) {
        try {
            String msg = "Free RAM size = " + roundBytesToGb(getFreeRamSize());
            if (LogLevel.DEBUG == logLevel) {
//                logger.debug(msg);
            } else if (LogLevel.INFO == logLevel) {
//                logger.info(msg);
            } else if (LogLevel.SYSOUT == logLevel) {
                System.out.println(msg);
            }
        } catch (Exception ignored) {}
    }

    private static String roundBytesToGb(Long bytes) {
        return String.format("%.2f", new Double(bytes) / (1024 * 1024 * 1024)) + "Gb";
    }

    private static Long getFreeRamSize() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        Method getFreePhysicalMemorySize = operatingSystemMXBean.getClass().getMethod("getFreePhysicalMemorySize");
        getFreePhysicalMemorySize.setAccessible(true);
        return (Long) getFreePhysicalMemorySize.invoke(operatingSystemMXBean);
    }
}
