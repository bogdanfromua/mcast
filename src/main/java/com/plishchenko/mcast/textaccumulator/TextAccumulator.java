package com.plishchenko.mcast.textaccumulator;

import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;

import static java.util.stream.Collectors.joining;

public class TextAccumulator {
    private Consumer<String> textChangesHandler;
    private Stack<String> accumulatedCharacters = new Stack<>();

    public TextAccumulator(Consumer<String> textChangesHandler) {
        this.textChangesHandler = textChangesHandler;
    }

    private static String charactersToString(List<String> chars) {
        return chars.stream()
                .map(Object::toString)
                .collect(joining());
    }

    public void push(String c) {
        accumulatedCharacters.push(c);
        notifyHandler();
    }

    public void pop() {
        if (!accumulatedCharacters.isEmpty()) {
            accumulatedCharacters.pop();
            notifyHandler();
        }
    }

    private void notifyHandler() {
        System.out.println(charactersToString(accumulatedCharacters));
        textChangesHandler.accept(charactersToString(accumulatedCharacters));
    }

    public void clear() {
        if (!accumulatedCharacters.isEmpty()) {
            accumulatedCharacters.clear();
            notifyHandler();
        }
    }

    public boolean isEmpty() {
        return accumulatedCharacters.isEmpty();
    }
}
